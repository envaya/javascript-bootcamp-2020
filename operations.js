

// == vs ===

// == doesnt care about type, only about value

// these return true:
console.warn('3' == 3);

console.warn(false == 0);

// --> go triple equals for everything