

// use typeof to log type
typeof ('hello');

// NAN

let NaNvariable = 0/0;
console.warn(NaNvariable);

let alsoNan = NaNvariable + 1;
console.warn(alsoNan);


// Javascript is a non-typed language

let iAmABoolean = false;
iAmABoolean = 111;

// Strings:
// strings are indexed:
let string1 = "chicken"
console.warn("index [0] of 'chicken': "+ string1[0]);
console.warn("length of 'chicken': "+ string1.length);

// strings are immutable:
let sport = 'Baseball';
// this does not alter sport
sport.slice(4);
console.warn(sport);


// will print "hi1"
"hi" + 1;

let tvShow = 'catdog';
console.warn("index of 'dog' in 'catdog': " +tvShow.indexOf('dog'));
// if a substring is not found in a string, indexOf will return -1

// UNDEFINED: