

// JS uses the Call Stack to keep track of what to run when

// when the script calls function1, the interpreter adds it to the call stack and then starty
// carrying out  function1

// when that function1 calls another function2, that function will then be added to the call stack further up
// and so forth until we arrive at the last bit of code that does not call any other code. that bit of code
// now lies on top of the stack and the interpreter starts to execute everything from top to bottom


//example:
// multiply will be on top of the stack

const multiply = (x, y) => x * y;

const square = (x) => multiply(x,x);

const isRightTriangle = (a,b,c) => {
  return square(a) + square(b) === square(c);
}


// JAVASCRIPT IS SINGLE THREADED
// ONLY ONE THING IS BEING RUN AT A TIME


console.warn('I happen first');
setTimeout(()=> {
  console.warn('I happen after 3 seconds');
  }, 3000);
console.warn('I happen second');

//the trick to work around this is the browser, the browser is actually written in C++
// the browser actually handles things such as requests and timing
// browser come with web apis that handle certain tasks in the background such as setTimeout
// in our example, javascript will pass the settimeout to the browser to handle
// the browser then keeps track of the timeout, but js moves on
// after 3 seconds the browser will put the code back onto the callstack
// same thing is happening with eventlistener. js is not listening constantly (it can only do 1 thing at a time),
// the browser keeps listening