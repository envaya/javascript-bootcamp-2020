const button = document.querySelector('button');

// basic idea of callback hell
// setTimeout(() => {
//   console.warn('moving button 100px, first callback');
//   button.style.transform = `translateX(100px)`;
//   setTimeout(() => {
//     console.warn('moving button additional 50px, nested callback');
//     button.style.transform = `translateX(150px)`;
//     setTimeout(() => {
//       button.style.transform = `translateX(200px)`;
//       setTimeout(() => {
//         button.style.transform = `translateX(300px)`;
//         console.warn('welcome to callback hell');
//       }, 1000);
//     }, 1000);
//   }, 1000);
// }, 1000);


const moveX = (element, distance, delay, callback) => {
  setTimeout(() => {
    element.style.transform = `translateX(${distance}px)`;
    if(callback) {
      callback();
    }
  }, delay);
}

moveX(button,300,3000, () => {
  moveX(button,100,1000);
});



